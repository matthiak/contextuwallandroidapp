# Android ContextuWall Client 

This is the Android client to be used with the *ContextuWall* system.
For details about this project go to our [project page](https://immersive-analytics.infotech.monash.edu/contextuwall) where you will also find the links to the other parts of the *ContextuWall* system.

This project was created with Android Studio.

## License ##
This project is Copyright Monash University, 2016 licensed under the [MIT License](https://opensource.org/licenses/MIT) or see `License.md`.

## Installation ##
* Clone the repository and build the application
or
* Using your Android device download a latest build [here](https://bitbucket.org/matthiak/contextuwallandroidapp/downloads/contextuwall.apk)

To install an application you have to enable the option to install third party apps on your Android device.

## Compatibility

The application is currently compatible with Android 5+

##Credits
Matthias Klapperstueck <matthias.klapperstueck@monash.edu>, Monash University