package de.vrd.contextuwall.ui;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import de.vrd.contextuwall.R;
import de.vrd.contextuwall.service.CWClientService;
import de.vrd.contextuwall.service.ContextuwallAPI;

/**
 * Created by matthiak on 23/06/2016.
 */
public class AnnotateFragment extends Fragment implements AnnotationSender {
    static String TAG = AnnotateFragment.class.getSimpleName();

    Bitmap image;
    ContextuwallAPI api;

    MyServiceListener slistener;

    String fileid;
    private ImageAnnotationView view;

    public AnnotateFragment() {
        slistener = new MyServiceListener();
    }

    public void setImage(String fileid) {
        Log.d(TAG, "[setImage] " + fileid);
        this.fileid = fileid;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_annotate, container, false);//super.onCreateView(inflater, container, savedInstanceState);
        view = (ImageAnnotationView)inflate.findViewById(R.id.annotationview);
        view.setSender(this);

        return inflate;
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        Intent intent = new Intent(getActivity().getApplicationContext(), CWClientService.class);
        getActivity().getApplicationContext().bindService(intent, slistener, Context.BIND_AUTO_CREATE);

    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        getActivity().getApplicationContext().unbindService(slistener);
    }

    @Override
    public synchronized void sendAnnotation(Bitmap annotation) {
        if(api != null) {
            Log.d(TAG, "[sendAnnotation]");
            //convert annotation to png
            //send annotation
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            annotation.compress(Bitmap.CompressFormat.PNG, 0, bos);
            final byte[] pngimage = bos.toByteArray();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    api.sendAnnotation(fileid, pngimage);
                }
            }).start();

        }
    }

    class MyServiceListener implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "Service bound..setting image");
            api = ((CWClientService.MyBinder)iBinder).getService().getCwAPI();

            view.setImage(api.getImageFromFileId(fileid));

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            api = null;
        }
    }


}
