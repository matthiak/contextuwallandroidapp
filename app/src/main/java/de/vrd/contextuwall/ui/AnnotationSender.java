package de.vrd.contextuwall.ui;

import android.graphics.Bitmap;

/**
 * Created by matthiak on 23/06/2016.
 */
public interface AnnotationSender {
    public void sendAnnotation(Bitmap annotation);
}
