package de.vrd.contextuwall.ui;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import de.vrd.contextuwall.R;

/**
 * Created by matthiak on 23/06/2016.
 */
public class AnnotateActivity extends Activity{
    static String TAG = AnnotateActivity.class.getSimpleName();
    public static final String FILEID = "FILEID";
    private AnnotateFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_annotate);

        fragment = (AnnotateFragment) getFragmentManager().findFragmentById(R.id.annotatefragmentcontainer);
        String fileid = getIntent().getExtras().getString(FILEID);
        if(fragment == null) {
            fragment = new AnnotateFragment();

            getFragmentManager().beginTransaction().add(R.id.annotatefragmentcontainer, fragment).commit();
        }
        Log.d(TAG, "Setting image" + fileid);
        fragment.setImage(fileid);

    }
}
