package de.vrd.contextuwall.ui;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v17.leanback.widget.HorizontalGridView;
import android.support.v17.leanback.widget.OnChildSelectedListener;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import de.vrd.contextuwall.adapter.GridViewImageAdapter;
import de.vrd.contextuwall.db.DBAccessor;
import de.vrd.contextuwall.db.SettingsDBHelper;
import de.vrd.contextuwall.service.CWClientService;
import de.vrd.contextuwall.R;
import de.vrd.contextuwall.service.ContextuwallAPI;

/**
 * Created by matthiak on 8/06/2016.
 */
public class MainFragment extends Fragment {

    private static final String TAG = MainFragment.class.getSimpleName();

    private Uri intentUri;

    TextView tvStatus;
    TextView tvURL;

    Bundle savedInstanceState;

    private CWClientService service;
    private ServiceConnectListener serviceConnectListener;

    private boolean isServiceConn = false;
    private ContextuwallAPI cwAPI;

    MyCWAPIListener callbacklistener;
    private GridViewImageAdapter gridViewImageAdapter;



    Map<String, ContextuwallAPI.CWImageObject> mapFileIdImageObject;

    ContextuwallAPI.CWImageObject curSelectedImageObject;
    private HorizontalGridView horizontalGridView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        Log.d(TAG, "savedInstanceState: " + savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        super.onCreate(savedInstanceState);

        setRetainInstance(true);



        Intent intent = getActivity().getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if(type.startsWith("image/")) {

                intentUri = (Uri)intent.getParcelableExtra(Intent.EXTRA_STREAM);
            }

        }

        serviceConnectListener = new ServiceConnectListener();

        mapFileIdImageObject = new HashMap<>();

        if(callbacklistener == null)
            callbacklistener = new MyCWAPIListener();

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        View fragmentview = inflater.inflate(R.layout.fragment_main, container, false);

        horizontalGridView = (HorizontalGridView)fragmentview.findViewById(R.id.horizontalgrid);
        horizontalGridView.setHorizontalMargin(10);
        gridViewImageAdapter = new GridViewImageAdapter(getActivity().getApplicationContext());
        horizontalGridView.setAdapter(gridViewImageAdapter);

        horizontalGridView.setOnChildSelectedListener(new OnChildSelectedListener() {
            @Override
            public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                Log.d(TAG, "selected image id: " + view.getTag());
                curSelectedImageObject = mapFileIdImageObject.get(view.getTag());
                if(cwAPI.isConnected() && view.getTag() != null) {
                    cwAPI.bringImageToFront((String) view.getTag());
                }
            }
        });


        ImageView iv = (ImageView) fragmentview.findViewById(R.id.toucharea);
        MyTouchListener myTouchListener = new MyTouchListener();
        iv.setOnTouchListener(myTouchListener);


        tvStatus = (TextView)fragmentview.findViewById(R.id.textViewStatus);
        tvURL = ((TextView)fragmentview.findViewById(R.id.textViewContent));
        return fragmentview;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");

        if(isServiceConn) {
            if(cwAPI != null)
                cwAPI.removeListener(callbacklistener);
            getActivity().getApplicationContext().unbindService(serviceConnectListener);
            isServiceConn = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        //callbacklistener.onClearWall();

        if(intentUri != null) {
            ((TextView) getView().findViewById(R.id.helptext)).setVisibility(View.GONE);
        }

        tvStatus.setText("Connecting to");
        tvStatus.setTextColor(Color.YELLOW);

        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Integer id = defaultSharedPreferences.getInt("selectedserverentry", -1);
        if(id == -1) {
            tvURL.setText("No server set up. Please go to settings and add a server");
        } else {
            SettingsDBHelper.ConnectionEntry connectionEntry = DBAccessor.getConnectionEntry(getActivity().getApplicationContext(), id);
            //String url = defaultSharedPreferences.getString("serverurl", "https://130.194.132.21:3000/");


            if (connectionEntry != null) {
                tvURL.setText(connectionEntry.values.get(SettingsDBHelper.ConnectionEntry.SERVER));
                Intent sIntent = new Intent(getActivity().getApplicationContext(), CWClientService.class);
                Log.d(TAG, "starting service");
                getActivity().getApplicationContext().startService(sIntent);
                Log.d(TAG, "binding service");
                if(!isServiceConn)
                    isServiceConn = getActivity().getApplicationContext().bindService(sIntent, serviceConnectListener, Context.BIND_AUTO_CREATE);
            }
            else
                tvURL.setText("No server set");


        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragmentmain_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_clearwall ) {
            cwAPI.clearWall();
            callbacklistener.onClearWall();
            return true;
        }
        if(cwAPI == null && !cwAPI.isConnected() || curSelectedImageObject == null)
            return false;
        if(item.getItemId() == R.id.menu_removeimage) {
            cwAPI.removeImage(curSelectedImageObject.fileid);
            callbacklistener.onRemoveImage(curSelectedImageObject.fileid);
            return true;
        } else if(item.getItemId() == R.id.menu_annotate) {
            if(curSelectedImageObject != null) {

                Intent intent = new Intent(getActivity().getApplicationContext(), AnnotateActivity.class);
                int idx = 0;
                intent.putExtra(AnnotateActivity.FILEID, curSelectedImageObject.fileid);
                startActivity(intent);
                return true;
            }
        }
        return false;
    }

    public void publishProgress(int value) {
        if(getView() != null) {
            ProgressBar pb = ((ProgressBar)getView().findViewById(R.id.progressBar));
            if(pb != null)
                pb.setProgress(value);

        }

    }




    class MyCWAPIListener implements ContextuwallAPI.CallbackListener {
        @Override
        public void onConnect() {
            callbacklistener.onClearWall();

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvStatus.setText("Connected to");
                    tvStatus.setTextColor(Color.GREEN);

                }
            });
            //cwAPI.requestModelFromServer();
        }

        @Override
        public void onProgress(Long value) {

        }

        @Override
        public void onIncomingImage(ContextuwallAPI.CWImageObject imageObject) {

            if(mapFileIdImageObject.get(imageObject.fileid) == null) {
                mapFileIdImageObject.put(imageObject.fileid, imageObject);

                final String fFileId = imageObject.fileid;
                final Bitmap image = imageObject.thumbnail;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gridViewImageAdapter.addImage(fFileId, image);
                        horizontalGridView.setSelectedPosition(gridViewImageAdapter.getItemCount() - 1);

                    }
                });

            }
        }

        @Override
        public void onDisconnect() {
            MainFragment.this.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvStatus.setText("Disconnected from");
                    tvStatus.setTextColor(Color.RED);
                }
            });
        }

        @Override
        public void onError(String error, String cause) {
            final String fError = error;
            MainFragment.this.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvStatus.setText("Connection Error");
                    tvStatus.setTextColor(Color.RED);
                    Toast.makeText(MainFragment.this.getActivity().getApplicationContext(), fError, Toast.LENGTH_SHORT).show();
                }
            });

        }

        @Override
        public void onImageSent(ContextuwallAPI.CWImageObject imageObject) {
            onIncomingImage(imageObject);
        }


        @Override
        public void onMoveImage(String fileid, double posx, double posy) {
            ContextuwallAPI.CWImageObject cwImageObject = mapFileIdImageObject.get(fileid);
            cwImageObject.normPosX = posx;
            cwImageObject.normPosY = posy;


        }

        @Override
        public void onResizeImage(String fileid, double scale) {
            ContextuwallAPI.CWImageObject cwImageObject = mapFileIdImageObject.get(fileid);

            cwImageObject.scale = scale;

        }
        @Override
        public void onRemoveImage(String fileid) {
            ContextuwallAPI.CWImageObject cwImageObject = mapFileIdImageObject.get(fileid);
            if(cwImageObject == null)
                return;
            if(curSelectedImageObject.equals(cwImageObject))
                curSelectedImageObject = null;

            mapFileIdImageObject.remove(fileid);

            final String fFileId = fileid;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    gridViewImageAdapter.removeImage(fFileId);
                }
            });

        }

        public void onClearWall() {
            mapFileIdImageObject.clear();
            curSelectedImageObject = null;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    gridViewImageAdapter.clearImageList();
                }
            });
        }

    }

    class ServiceConnectListener implements ServiceConnection {


        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "[onServiceConnected]");
            service = ((CWClientService.MyBinder) iBinder).getService();
            cwAPI = service.getCwAPI();

            cwAPI.addListener(callbacklistener);

            if(cwAPI.isConnected()) {
                tvStatus.setText("Connected to");
                tvStatus.setTextColor(Color.GREEN);

                ContextuwallAPI.CWImageObject temp = curSelectedImageObject;
                // reset out local model
                callbacklistener.onClearWall();
                for(ContextuwallAPI.CWImageObject curObject : cwAPI.requestModel()) {
                    callbacklistener.onIncomingImage(curObject);
                    if(temp != null && curObject.fileid.equals(temp.fileid))
                        curSelectedImageObject = curObject;
                }
            }

            if(intentUri != null) {
                service.sendImage(intentUri);
                intentUri = null;
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "[onServiceDisconnected]");
        }
    }

    class MyTouchListener extends ScaleGestureDetector.SimpleOnScaleGestureListener
            implements View.OnTouchListener {
        double prevX;
        double prevY;

        boolean actiondown = false;

        ScaleGestureDetector sgd;


        public MyTouchListener() {
            sgd = new ScaleGestureDetector(MainFragment.this.getActivity().getApplicationContext(), this);
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if( sgd.onTouchEvent(motionEvent)&& motionEvent.getPointerCount() != 1) {
                actiondown = false;
                return true;
            }

            if(motionEvent.getPointerCount() != 1)
                return false;
            if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                prevX = motionEvent.getX();
                prevY = motionEvent.getY();
                actiondown = true;
                Log.d(TAG, "actiondown: posx/y: " + prevX + "/" + prevY + " #pointer:" + motionEvent.getPointerCount());
            } else if(motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                if(!actiondown)
                    return true;
                if(curSelectedImageObject != null && cwAPI != null && cwAPI.isConnected()) {
                    double diffX = (motionEvent.getX() - prevX) / 1000;
                    double diffY = (motionEvent.getY() - prevY) / 1000;
                    //Log.d(TAG, "actionmove: diffx/y: " + diffX + "/" + diffY);

                    if(diffX == 0 && diffY == 0)
                        return true;

                    curSelectedImageObject.normPosX += diffX;
                    curSelectedImageObject.normPosY += diffY;
                    if(curSelectedImageObject.normPosX > 1)
                        curSelectedImageObject.normPosX = 1;
                    if(curSelectedImageObject.normPosY > 1)
                        curSelectedImageObject.normPosY = 1;
                    if(curSelectedImageObject.normPosX < 0)
                        curSelectedImageObject.normPosX = 0;
                    if(curSelectedImageObject.normPosY < 0)
                        curSelectedImageObject.normPosY = 0;
                    Log.d(TAG, "actionmove: posx/y: " + curSelectedImageObject.normPosX + "," + curSelectedImageObject.normPosY);

                    cwAPI.moveImage(
                            curSelectedImageObject.fileid,
                            curSelectedImageObject.normPosX,
                            curSelectedImageObject.normPosY);

                    prevX = motionEvent.getX();
                    prevY = motionEvent.getY();
                }
            }

            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            //return super.onScale(detector);
            float scale = detector.getPreviousSpan() - detector.getCurrentSpan();
            if(scale < 0)
                curSelectedImageObject.scale *= 1.01;
            else
                curSelectedImageObject.scale /= 1.01;

            if(curSelectedImageObject.scale < 0.01)
                curSelectedImageObject.scale = 0.01;

            cwAPI.scaleImage(
                    curSelectedImageObject.fileid,
                    curSelectedImageObject.scale
            );

            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return super.onScaleBegin(detector);
        }
    }
}
