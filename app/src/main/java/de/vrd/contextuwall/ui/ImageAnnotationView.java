package de.vrd.contextuwall.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import de.vrd.contextuwall.service.ContextuwallAPI;

/**
 * Created by matthiak on 23/06/2016.
 */
public class ImageAnnotationView extends View {
    static String TAG = ImageAnnotationView.class.getSimpleName();

    private Paint paint;
    ContextuwallAPI.CWImageObject image;

    float oldx;
    float oldy;
    float newx;
    float newy;

    Bitmap annotation;
    private Canvas c;
    private Matrix transformImageScale;
    private float imagescale;
    private Matrix transformAnnotationScale;
    private float annotationscale;

    AnnotationSender sender;

    public ImageAnnotationView(Context context) {
        super(context);

    }

    public ImageAnnotationView(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint = new Paint();
        paint.setColor(Color.RED);

        this.setOnTouchListener(new MyTouchListener());

    }

    public void setSender(AnnotationSender sender) {
        this.sender = sender;
    }

    public void setImage(ContextuwallAPI.CWImageObject image) {
        this.image = image;

        float widthscale = (float)getWidth() / (float)this.image.thumbnail.getWidth();
        float heightscale = (float)getHeight() / (float)this.image.thumbnail.getHeight();
        imagescale = widthscale > heightscale ? heightscale : widthscale;

        float awidthscale = (float)getWidth() / (float)this.image.width;
        float aheightscale = (float)getHeight() / (float)this.image.height;
        annotationscale = awidthscale > aheightscale ? aheightscale : awidthscale;


        float aspect = (float)this.image.thumbnail.getWidth() / (float)this.image.thumbnail.getHeight();

        if(this.image.annotation == null)
//            annotation = Bitmap.createBitmap((int)this.image.width, (int)(this.image.width / aspect), image.thumbnail.getConfig());
            annotation = Bitmap.createBitmap(getWidth(), (int)((float)getWidth() / aspect), image.thumbnail.getConfig());
        else
            annotation = BitmapFactory.decodeByteArray(this.image.annotation, 0, this.image.annotation.length, null).copy(this.image.thumbnail.getConfig(), true);
        c = new Canvas(annotation);

        Log.d(TAG, "annotation has size: " + annotation.getWidth() + ", " + annotation.getHeight());

        transformImageScale = new Matrix();
        transformImageScale.setScale(imagescale, imagescale);

        transformAnnotationScale = new Matrix();
        transformAnnotationScale.setScale(annotationscale, annotationscale);

        invalidate();
        Log.d(TAG, "Bitmap set");
    }

    @Override
    public void draw(Canvas canvas) {
        //Log.d(TAG, "[draw]");
        super.draw(canvas);
        c.drawLine(oldx, oldy, newx, newy, paint);

        oldx = newx;
        oldy = newy;

        if(this.image != null) {
            //Log.d(TAG, "[draw].. image");
            canvas.save();
            canvas.concat(transformImageScale);
            canvas.drawBitmap(this.image.thumbnail, 0, 0, null);
            canvas.restore();
            canvas.drawBitmap(this.annotation, 0, 0, null);
        }
        //canvas.drawLine(0f, 0f, 300f, 300f, paint);

    }

    class MyTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            Log.d(TAG, "touch event");
            if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                oldx = motionEvent.getX();
                oldy = motionEvent.getY();
            }
            if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if(ImageAnnotationView.this.sender != null) {
                    ImageAnnotationView.this.sender.sendAnnotation(annotation);
                }
            }
            if(motionEvent.getAction() == MotionEvent.ACTION_MOVE) {

                newx = motionEvent.getX();
                newy = motionEvent.getY();

                invalidate();
            }
            return true;
        }
    }
}
