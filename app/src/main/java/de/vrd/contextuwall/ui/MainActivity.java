package de.vrd.contextuwall.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import de.vrd.contextuwall.service.CWClientService;
import de.vrd.contextuwall.R;

public class MainActivity extends Activity {
    public static final String TAG = MainActivity.class.getSimpleName();


    MainFragment mainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        Log.d(TAG, "savedInstanceState: " + savedInstanceState);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment f = getFragmentManager().findFragmentById(R.id.mainfragmentcontainer);

        if(f == null) {
            mainFragment = new MainFragment();
            mainFragment.setRetainInstance(true);
            getFragmentManager().beginTransaction().add(R.id.mainfragmentcontainer, mainFragment).commit();
            Log.d(TAG, "adding fragment");
        } else {
            Log.d(TAG, "not adding fragment");
            if(f instanceof MainFragment)
                mainFragment = (MainFragment)f;
        }

        getApplicationContext().registerReceiver(new MyBroadCastReceiver(), new IntentFilter(CWClientService.ACTION_PROGRESS));

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        //actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(R.mipmap.contextucallicon);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //return super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.menu_settings_id) {
            Intent i = new Intent();
            i.setClass(getApplicationContext(), SettingsActivity.class);
            startActivity(i);
            return true;
        }
        return false;
    }

    class MyBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(CWClientService.ACTION_PROGRESS.equals(intent.getAction())) {
                Log.d(TAG, "broadcast: onReceive: received new progress value");
                if (mainFragment != null) {
                    Long value = intent.getLongExtra("progress", 0);
                    mainFragment.publishProgress(value.intValue());
                }
            }

        }
    }
}
