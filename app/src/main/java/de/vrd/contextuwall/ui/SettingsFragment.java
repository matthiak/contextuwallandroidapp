package de.vrd.contextuwall.ui;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import de.vrd.contextuwall.R;
import de.vrd.contextuwall.db.DBAccessor;
import de.vrd.contextuwall.db.SettingsDBHelper;
import de.vrd.contextuwall.service.CWClientService;
import de.vrd.contextuwall.service.Tools;

/**
 * Created by matthiak on 7/06/2016.
 */
public class SettingsFragment extends Fragment{
    private static final String TAG = SettingsFragment.class.getSimpleName();
    private List<SettingsDBHelper.ConnectionEntry> connectionEntries;

    private SettingsDBHelper.ConnectionEntry selectedEntry;

    EditText edittextConnName;
    EditText edittextServer;
    EditText edittextPort;
    EditText edittextPassword;
    private ArrayAdapter<SettingsDBHelper.ConnectionEntry> arrayAdapterAvailConnections;
    private Spinner spinner;

    boolean newEntry = false;

    private ArrayAdapter<Bitmap.CompressFormat> arrayAdapterFileType;
    private Spinner spinnerImageType;
    List<Bitmap.CompressFormat> listFileTypes = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    //    addPreferencesFromResource(R.xml.settings);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);

        edittextConnName = ((EditText)view.findViewById(R.id.settingConnectionName));
        edittextServer = ((EditText)view.findViewById(R.id.settingServerName));
        edittextPort = ((EditText)view.findViewById(R.id.settingServerPort));
        edittextPassword = ((EditText)view.findViewById(R.id.settingServerPassword));


        ((Button)view.findViewById(R.id.setButSave)).setOnClickListener(new SaveButtonListener());
        ((ImageButton)view.findViewById(R.id.setButNew)).setOnClickListener(new NewButtonListener());

        spinner = ((Spinner)view.findViewById(R.id.settingListConnections));

        connectionEntries = DBAccessor.getConnectionEntries(getActivity().getApplicationContext());

        arrayAdapterAvailConnections = new ArrayAdapter<>(getActivity().getApplicationContext(), R.layout.spinneritem, /* android.R.layout.simple_spinner_item,*/ connectionEntries);
        arrayAdapterAvailConnections.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapterAvailConnections);

        spinner.setOnItemSelectedListener(new ItemSelectListener());

        edittextPort.setText(Integer.toString(3000));
//        spinnerImageType = (Spinner)view.findViewById(R.id.settingsFileType);
//        arrayAdapterFileType = new ArrayAdapter<>(getActivity().getApplicationContext(), R.layout.spinneritem, Bitmap.CompressFormat.values());
//        arrayAdapterFileType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerImageType.setAdapter(arrayAdapterAvailConnections);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        Integer id = defaultSharedPreferences.getInt("selectedserverentry", -1);
        if(id != -1) {
            int idx = 0;
            for (SettingsDBHelper.ConnectionEntry entry : connectionEntries) {
                if (entry.getId() == id) {
                    break;
                }
                idx++;
            }
            spinner.setSelection(idx);
        }
    }

    private void setViewText(SettingsDBHelper.ConnectionEntry entry) {
        Log.d(TAG, "setting edit textviews");
        edittextConnName.setText(entry.values.get(SettingsDBHelper.ConnectionEntry.CONNNAME));
        edittextServer.setText(entry.values.get(SettingsDBHelper.ConnectionEntry.SERVER));
        edittextPort.setText(entry.values.get(SettingsDBHelper.ConnectionEntry.PORT));
        edittextPassword.setText(entry.values.get(SettingsDBHelper.ConnectionEntry.PASSWORD));

    }
    private void getViewText(SettingsDBHelper.ConnectionEntry entry) {

        entry.values.put(SettingsDBHelper.ConnectionEntry.CONNNAME, edittextConnName.getText().toString().trim());
        entry.values.put(SettingsDBHelper.ConnectionEntry.SERVER, edittextServer.getText().toString().trim());
        entry.values.put(SettingsDBHelper.ConnectionEntry.PORT, edittextPort.getText().toString().trim());

        String password = edittextPassword.getText().toString().trim();
        // only create md5, if password has changed
        if( ! password.equals(entry.values.get(SettingsDBHelper.ConnectionEntry.PASSWORD))) {
            String md5String = Tools.getMd5String(password);
            Log.d(TAG, "password changed: new: '" + password + "'. creating md5: " + md5String );
            entry.values.put(SettingsDBHelper.ConnectionEntry.PASSWORD, md5String);
        } else {
            Log.d(TAG, "keeping old password:" + password );
            entry.values.put(SettingsDBHelper.ConnectionEntry.PASSWORD, password);
        }

    }


    class ItemSelectListener implements Spinner.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Log.d(TAG, "[onItemSelected] i=" + i + ", l=" + l);
            selectedEntry = connectionEntries.get(i);
            setViewText(selectedEntry);
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            SharedPreferences.Editor edit = defaultSharedPreferences.edit();
            Log.d(TAG, "selected entry id " + selectedEntry.getId());
            edit.putInt("selectedserverentry",selectedEntry.getId() );
            edit.commit();
            Intent intent = new Intent(getActivity().getApplicationContext(), CWClientService.class);
            getActivity().getApplicationContext().stopService(intent);

            newEntry = false;
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            Log.d(TAG, "[onNothingSelected]");

        }
    }

    class SaveButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if(selectedEntry == null) {
                newEntry = true;
                selectedEntry = new SettingsDBHelper.ConnectionEntry(0);
            }
            getViewText(selectedEntry);
            if( ! newEntry /*selectedEntry.getId() != SettingsDBHelper.ConnectionEntry.NOID*/)
                DBAccessor.updateConnectionEntry(getActivity().getApplicationContext(), selectedEntry);
            else {
                DBAccessor.addConnectionEntry(getActivity().getApplicationContext(), selectedEntry);
                connectionEntries = DBAccessor.getConnectionEntries(getActivity().getApplicationContext());
                arrayAdapterAvailConnections.clear();
                arrayAdapterAvailConnections.addAll(connectionEntries);
                int idx = 0;
                for(SettingsDBHelper.ConnectionEntry ce : connectionEntries) {

                    if (ce.values.get(SettingsDBHelper.ConnectionEntry.CONNNAME).equals(selectedEntry.values.get(SettingsDBHelper.ConnectionEntry.CONNNAME))) {
                        spinner.setSelection(idx, true); // animate = true also triggers onitemselected (??!? ANDROID broken)
                        break;
                    }
                    idx++;
                }

            }
            Intent intent = new Intent(getActivity().getApplicationContext(), CWClientService.class);
            getActivity().getApplicationContext().stopService(intent);

            newEntry = false;
        }
    }
    class NewButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Log.d(TAG, "new entry");
            newEntry = true;
            selectedEntry = new SettingsDBHelper.ConnectionEntry(0);
            selectedEntry.values.put(SettingsDBHelper.ConnectionEntry.PORT, Integer.toString(3000));
            setViewText(selectedEntry);
        }
    }
}
