package de.vrd.contextuwall.ui;

import android.app.Activity;
import android.os.Bundle;

import de.vrd.contextuwall.R;

/**
 * Created by matthiak on 7/06/2016.
 */
public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_activity);

        getFragmentManager().beginTransaction().add(R.id.settingscontainer, new SettingsFragment()).commit();

        getActionBar().setTitle("Settings");
    }
}
