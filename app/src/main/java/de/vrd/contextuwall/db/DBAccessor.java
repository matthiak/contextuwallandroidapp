package de.vrd.contextuwall.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.audiofx.BassBoost;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.vrd.contextuwall.service.Tools;

/**
 * Created by matthiak on 10/06/2016.
 */
public class DBAccessor {

    private final static String TAG = DBAccessor.class.getSimpleName();

    private static final String[] projection = {
        SettingsDBHelper.ConnectionEntry._ID,
        SettingsDBHelper.ConnectionEntry.CONNNAME,
        SettingsDBHelper.ConnectionEntry.SERVER,
        SettingsDBHelper.ConnectionEntry.PORT,
        SettingsDBHelper.ConnectionEntry.PASSWORD,
    };



    private static SQLiteDatabase database;

    private static SQLiteDatabase getDatabase(Context context) {
        if(database == null) {
            SettingsDBHelper helper = new SettingsDBHelper(context);
            database = helper.getWritableDatabase();
        }
        return database;
    }

    public static List<SettingsDBHelper.ConnectionEntry> getConnectionEntries(Context context) {
        Log.d(TAG, "[getConnectionEntries] querying database");
        Cursor query = getDatabase(context).query(
                SettingsDBHelper.TABLE_SERVERS,
                projection,
                null,
                null,
                null,
                null,
                SettingsDBHelper.ConnectionEntry.CONNNAME + " ASC"
        );

        List<SettingsDBHelper.ConnectionEntry> retList = new ArrayList<>();
        //SettingsDBHelper.ConnectionEntry entry = new SettingsDBHelper.ConnectionEntry(SettingsDBHelper.ConnectionEntry.NOID);

        //entry.values.put(SettingsDBHelper.ConnectionEntry.CONNNAME, "New");
        //retList.add(entry);
        if(query.moveToFirst() )
            while(query.moveToNext())
                retList.add(fillEntryObject(query));

        Log.d(TAG, "[getConnectionEntries] returning " + retList.size() + " elements");
        return retList;
    }

    public static void updateConnectionEntry(Context context, SettingsDBHelper.ConnectionEntry entry) {
        Log.d(TAG, "[updateConnectionEntry] for entry name: " + entry.values.get(SettingsDBHelper.ConnectionEntry.CONNNAME)
                + " " + entry.values.get(SettingsDBHelper.ConnectionEntry.PORT) + " " + entry.values.get(SettingsDBHelper.ConnectionEntry.PASSWORD));
        ContentValues values = new ContentValues();
        values.put(SettingsDBHelper.ConnectionEntry.CONNNAME, entry.values.get(SettingsDBHelper.ConnectionEntry.CONNNAME));
        values.put(SettingsDBHelper.ConnectionEntry.SERVER, entry.values.get(SettingsDBHelper.ConnectionEntry.SERVER));
        values.put(SettingsDBHelper.ConnectionEntry.PORT, entry.values.get(SettingsDBHelper.ConnectionEntry.PORT));
        values.put(SettingsDBHelper.ConnectionEntry.PASSWORD, entry.values.get(SettingsDBHelper.ConnectionEntry.PASSWORD));
        getDatabase(context).update(
                SettingsDBHelper.TABLE_SERVERS,
                values,
                SettingsDBHelper.ConnectionEntry._ID + " = ? ",
                new String[] {Integer.toString(entry.getId())}
        );

    }

    public static void addConnectionEntry(Context context, SettingsDBHelper.ConnectionEntry entry) {
        Log.d(TAG, "[addConnectionEntry] for entry name: " + entry.values.get(SettingsDBHelper.ConnectionEntry.CONNNAME)
                + " " + entry.values.get(SettingsDBHelper.ConnectionEntry.PORT) + " " + entry.values.get(SettingsDBHelper.ConnectionEntry.PASSWORD));
        ContentValues values = new ContentValues();
        values.put(SettingsDBHelper.ConnectionEntry.CONNNAME, entry.values.get(SettingsDBHelper.ConnectionEntry.CONNNAME));
        values.put(SettingsDBHelper.ConnectionEntry.SERVER, entry.values.get(SettingsDBHelper.ConnectionEntry.SERVER));
        values.put(SettingsDBHelper.ConnectionEntry.PORT, entry.values.get(SettingsDBHelper.ConnectionEntry.PORT));
        values.put(SettingsDBHelper.ConnectionEntry.PASSWORD, entry.values.get(SettingsDBHelper.ConnectionEntry.PASSWORD));
        getDatabase(context).insert(
                SettingsDBHelper.TABLE_SERVERS,
                null,
                values
        );

    }

    public static SettingsDBHelper.ConnectionEntry getConnectionEntry(Context context, int id) {
        Log.d(TAG, "[getConnectionEntry] for entry id: " + id);
        SettingsDBHelper.ConnectionEntry entry = new SettingsDBHelper.ConnectionEntry(id);
        Cursor query = getDatabase(context).query(
                SettingsDBHelper.TABLE_SERVERS,
                projection,
                SettingsDBHelper.ConnectionEntry._ID + " = ? ",
                new String[] {Integer.toString(id)},
                null,
                null,
                null
        );
        if(query.moveToFirst())
            return fillEntryObject(query);
        else
            return null;
    }

    private static SettingsDBHelper.ConnectionEntry  fillEntryObject(Cursor query) {
        SettingsDBHelper.ConnectionEntry entry;
                int id = query.getInt(query.getColumnIndex(SettingsDBHelper.ConnectionEntry._ID));
                entry = new SettingsDBHelper.ConnectionEntry(id);

                entry.values.put(
                        SettingsDBHelper.ConnectionEntry.CONNNAME,
                        query.getString(query.getColumnIndex(SettingsDBHelper.ConnectionEntry.CONNNAME))
                );
                entry.values.put(
                        SettingsDBHelper.ConnectionEntry.SERVER,
                        query.getString(query.getColumnIndex(SettingsDBHelper.ConnectionEntry.SERVER))
                );
                entry.values.put(
                        SettingsDBHelper.ConnectionEntry.PORT,
                        query.getString(query.getColumnIndex(SettingsDBHelper.ConnectionEntry.PORT))
                );
                entry.values.put(
                        SettingsDBHelper.ConnectionEntry.PASSWORD,
                        query.getString(query.getColumnIndex(SettingsDBHelper.ConnectionEntry.PASSWORD))
                );

                return entry;


    }
}
