package de.vrd.contextuwall.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import de.vrd.contextuwall.service.Tools;

/**
 * Created by matthiak on 10/06/2016.
 */
public class SettingsDBHelper extends SQLiteOpenHelper {

    private final static String TAG = SettingsDBHelper.class.getSimpleName();

    public static class ConnectionEntry implements BaseColumns {
        public static final int NOID = -1;

        public static final String CONNNAME = "connname";
        public static final String SERVER = "server";
        public static final String PORT = "port";
        public static final String PASSWORD = "password";

        public Map<String, String> values;

        private int id;

        public ConnectionEntry() {
            this(-1);
            values.put(CONNNAME, "");
            values.put(SERVER, "");
            values.put(PORT, "");
            values.put(PASSWORD, "");
        }

        public ConnectionEntry(int id) {
            this.values = new HashMap<>();
            this.id = id;
        }

        public int getId() {
            return id;
        }

        @Override
        public String toString() {

            return this.values.get(CONNNAME);
        }
    }

    Context context;

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_FILE = "ServerConnections.db";


    public static String TABLE_SERVERS = "servers";

    private static final String SQL_CREATE_TABLE =
                    "CREATE TABLE " + TABLE_SERVERS + " (" +
                     ConnectionEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                     ConnectionEntry.CONNNAME + " TEXT, " +
                     ConnectionEntry.SERVER + " TEXT, " +
                     ConnectionEntry.PORT + " TEXT, " +
                     ConnectionEntry.PASSWORD + " TEXT " +
                    " )";

    private static final String SQL_DELETE_DB =
            "DROP TABLE IF EXISTS " + TABLE_SERVERS;



    public SettingsDBHelper(Context context) {
        super(context, DATABASE_FILE, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(TAG, "[onCreate]");
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE);

        createDefaultEntries(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_DB);
        onCreate(sqLiteDatabase);
    }

    private void createDefaultEntries(SQLiteDatabase sqLiteDatabase) {
        ConnectionEntry entry = new ConnectionEntry(0);
        entry.values.put(ConnectionEntry.CONNNAME, "Default Conn");
        entry.values.put(ConnectionEntry.SERVER, "130.194.132.21");
        entry.values.put(ConnectionEntry.PORT, "3000");
        entry.values.put(ConnectionEntry.PASSWORD, Tools.getMd5String("foobar"));
        ContentValues values = new ContentValues();
        values.put(SettingsDBHelper.ConnectionEntry.CONNNAME, entry.values.get(SettingsDBHelper.ConnectionEntry.CONNNAME));
        values.put(SettingsDBHelper.ConnectionEntry.SERVER, entry.values.get(SettingsDBHelper.ConnectionEntry.SERVER));
        values.put(SettingsDBHelper.ConnectionEntry.PORT, entry.values.get(SettingsDBHelper.ConnectionEntry.PORT));
        values.put(SettingsDBHelper.ConnectionEntry.PASSWORD, entry.values.get(SettingsDBHelper.ConnectionEntry.PASSWORD));

        sqLiteDatabase.insert(
                SettingsDBHelper.TABLE_SERVERS,
                null,
                values
        );

    }
}
