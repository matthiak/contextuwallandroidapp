package de.vrd.contextuwall.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.vrd.contextuwall.R;

/**
 * Created by matthiak on 13/06/2016.
 */
public class GridViewImageAdapter extends RecyclerView.Adapter<GridViewImageAdapter.MyImageViewHolder>
    implements View.OnClickListener, View.OnFocusChangeListener
{

    private List<FileidImage> listFileIdImages;


    class FileidImage {
        String fileId;
        Bitmap bitmap;

        public FileidImage(String fileId, Bitmap bitmap) {
            this.bitmap = bitmap;
            this.fileId = fileId;
        }
    }

    public interface ItemClickListener {
        public void onItemClick(View item);
    }
    ItemClickListener itemClickListener;

    //private final Bitmap bitmap;

    public GridViewImageAdapter(Context context) {

        //bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.contextuwall);

        listFileIdImages = new ArrayList<>();
    }

    public static class MyImageViewHolder extends RecyclerView.ViewHolder {

        ImageView im;

        public MyImageViewHolder(ImageView itemView) {
            super(itemView);
            this.im = itemView;
        }
    }


    @Override
    public int getItemCount() {

        return listFileIdImages.size();
    }

    @Override
    public GridViewImageAdapter.MyImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ImageView inflate = (ImageView)LayoutInflater.from(parent.getContext()).inflate(R.layout.horizgridimageviewitem_layout, parent, false);
        inflate.setFocusable(true);
        inflate.setFocusableInTouchMode(true);
        inflate.setOnClickListener(this);
        inflate.setOnFocusChangeListener(this);

        return new MyImageViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(GridViewImageAdapter.MyImageViewHolder holder, int position) {
        FileidImage fileidImage = listFileIdImages.get(position);
        holder.im.setImageBitmap(fileidImage.bitmap);
        holder.im.setTag(fileidImage.fileId);

    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(b) {
            view.setBackgroundColor(Color.argb(255,220,220,50));
        } else {
            view.setBackgroundColor(Color.argb(255,0, 0, 0));
        }
    }

    @Override
    public void onClick(View view) {
        // taken from the example activity for HorizontalGridView
        notifyDataSetChanged();
    }

    public void addImage(String fileid, Bitmap image) {
        listFileIdImages.add(new FileidImage(fileid, image));
        notifyDataSetChanged();

    }

    public void removeImage(String fileid) {
        Iterator<FileidImage> iterator = listFileIdImages.iterator();
        while(iterator.hasNext()) {
            FileidImage next = iterator.next();
            if(next.fileId.equals(fileid)) {
                listFileIdImages.remove(next);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void clearImageList() {
        listFileIdImages.clear();
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(ItemClickListener icl) {

        this.itemClickListener = icl;
    }
}
