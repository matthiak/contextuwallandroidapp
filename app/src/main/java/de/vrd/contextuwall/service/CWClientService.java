package de.vrd.contextuwall.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import de.vrd.contextuwall.db.DBAccessor;
import de.vrd.contextuwall.db.SettingsDBHelper;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by matthiak on 9/06/2016.
 */
public class CWClientService extends Service
		implements ContextuwallAPI.CallbackListener {

	public static final String ACTION_PROGRESS = "de.vrd.contextuwall.PROGRESS";

	private static final String TAG = CWClientService.class.getSimpleName();

	public static class StateObj {
		public enum STATE {
			ENQUEUED,
			READY,
			UPLOADING,
			FINISHED
		}

		public String fileId;
		public STATE state;
		public Uri fileUri;
	}


	public class MyBinder extends Binder {
		public CWClientService getService() {
			return CWClientService.this;
		}
	}

	MyBinder mBinder;

	//Socket socket;

	Map<String, StateObj> mapFileIdStateObj;

	SendImageThread sendImageThread;

	ContextuwallAPI cwAPI;

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate");
		mBinder = new MyBinder();

		mapFileIdStateObj = new HashMap<>();

		//initCWAPI();

	}

	private void initCWAPI() {

		SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		Integer id = defaultSharedPreferences.getInt("selectedserverentry", -1);
		SettingsDBHelper.ConnectionEntry entry = DBAccessor.getConnectionEntry(getApplicationContext(), id);
            /*
			init connection will also create the sendimagethread, if the connection was successfully
            established
             */
		if(entry != null) {
			cwAPI = new ContextuwallAPI("https://" + entry.values.get(SettingsDBHelper.ConnectionEntry.SERVER) + ":" + entry.values.get(SettingsDBHelper.ConnectionEntry.PORT),
					entry.values.get(SettingsDBHelper.ConnectionEntry.PASSWORD), getApplicationContext(), this);

			cwAPI.connect();
		}
	}


	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand");
		if( cwAPI == null || !(cwAPI.isConnecting() || cwAPI.isConnected()) ) {
			Log.d(TAG, "initialising connection");
			initCWAPI();
		}
		return START_STICKY;

	}

	public ContextuwallAPI getCwAPI() {
		return cwAPI;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy.. closing connection");
		if (cwAPI != null && cwAPI.isConnected()) {
			sendImageThread.setLeave(true);
			cwAPI.disconnect();
		}

	}

	/* 	===========

	CALLBACKS FROM CWAPI

		=========== */
	@Override
	public void onProgress(Long value) {

		Intent intent = new Intent(ACTION_PROGRESS);
		intent.putExtra("progress", value);
		sendBroadcast(intent);

	}

	@Override
	public void onConnect() {

		if (sendImageThread != null && sendImageThread.isAlive()) {
			Log.d(TAG, "sending thread is still alive, sending an image... set 'leave' to false");
			sendImageThread.setLeave(false);
		} else {
			Log.d(TAG, "no send thread found: creating");
			sendImageThread = new SendImageThread();
			sendImageThread.start();
		}
	}

	@Override
	public void onDisconnect() {

		stopSelf();
	}

	@Override
	public void onError(String error, String cause) {

		stopSelf();
	}

	@Override
	public void onImageSent(ContextuwallAPI.CWImageObject imageObject) {}

	@Override
	public void onIncomingImage(ContextuwallAPI.CWImageObject imageObject) {}

	@Override
	public void onMoveImage(String fileid, double posx, double posy) {}

	@Override
	public void onRemoveImage(String fileid) {}

	@Override
	public void onResizeImage(String fileid, double scale) {}

	public void onClearWall() {}

	/* 	===========

	END CALLBACKS FROM CWAPI

		=========== */

	public StateObj getStateObj(String fileId) {
		return mapFileIdStateObj.get(fileId);
	}

	public String sendImage(Uri imageUri) {
		Log.d(TAG, "sendImage: sending: " + imageUri.toString());
		StateObj obj;

		obj = new StateObj();
		obj.state = StateObj.STATE.ENQUEUED;
		obj.fileId = UUID.randomUUID().toString();// Tools.getMd5String(new Long(System.currentTimeMillis()).toString());
		obj.fileUri = imageUri;
		Log.d(TAG, "fileid: " + obj.fileId);
		synchronized (mapFileIdStateObj) {
			mapFileIdStateObj.put(obj.fileId, obj);
		}

		if (cwAPI == null || (!cwAPI.isConnected() && ! cwAPI.isConnecting())) {
			Log.d(TAG, "sendImage: not connected... creating new connection");
			initCWAPI();

		} else if (sendImageThread != null && !sendImageThread.isAlive() && cwAPI.isConnected()) {
			Log.d(TAG, "sendImage: still connected.. appending to list for sending images");
            /*
            the socket is still active, but the send image thread finished it's work
            uploading all the images in the queue
            Start a new upload thread
             */
			sendImageThread = new SendImageThread();
			sendImageThread.start();
		}
		return obj.fileId;
	}




	class SendImageThread extends Thread {

		boolean leave = false;

		public void setLeave(boolean leave) {
			this.leave = leave;
		}

		@Override
		public void run() {
			Log.d(TAG, "[SendImageThread] starting uploading all images in queue");
			while (!mapFileIdStateObj.isEmpty() && !leave) {

				StateObj curStateObj;
				synchronized (mapFileIdStateObj) {
					curStateObj = mapFileIdStateObj.get(mapFileIdStateObj.keySet().iterator().next());
				}
				curStateObj.state = StateObj.STATE.UPLOADING;
				cwAPI.cwSendImage(curStateObj.fileUri, curStateObj.fileId);
				synchronized (mapFileIdStateObj) {
					mapFileIdStateObj.remove(curStateObj.fileId);
				}

			}
			Log.d(TAG, "[SendImageThread] finished uploading all images in queue");
		}


	}
}
