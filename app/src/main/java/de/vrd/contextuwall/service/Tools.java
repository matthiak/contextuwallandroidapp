package de.vrd.contextuwall.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by matthiak on 7/06/2016.
 */
public class Tools {

    public static String getMd5String(String text) {
        try {
            StringBuffer sb = new StringBuffer();
            byte[] digest = MessageDigest.getInstance("MD5").digest(text.getBytes());
            for (int i = 0; i < digest.length; i++) {
                String hex = Integer.toHexString(digest[i] & 0xFF);
                if (hex.length() == 1)
                    hex = "0" + hex;
                sb.append(hex);
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {


        }
        return null;
    }
}
