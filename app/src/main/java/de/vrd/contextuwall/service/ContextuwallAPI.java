package de.vrd.contextuwall.service;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.client.SocketIOException;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.EngineIOException;

/**
 * Created by matthiak on 10/06/2016.
 */
public class ContextuwallAPI {
    private static final String TAG = ContextuwallAPI.class.getSimpleName();

    private static final String CW_ONAUTHENTICATEOK = "authenticate_ok";
    private static final String CW_INCOMINGSHADOWIMAGE = "incomingShadowImage";
    private static final String CW_RESIZEIMAGE = "resizeImage";
    private static final String CW_MOVEIMAGE = "updateImageLocation";
    private static final String CW_REMOVEIMAGE = "removeImage";
    private static final String CW_CLEARWALL = "clearWall";

    List<CallbackListener> listcallbacks;
    private boolean authenticate_ok;
    private boolean isconnecting;

    List<CWImageObject> imageObjectCache;

    String cwannotid;

    public interface CallbackListener {

        /**
         * this method is called, when we actually have a working
         * connection to the server, after the authentication was
         * successful.
         *
         */
        public void onConnect();
        public void onProgress(Long value);
        public void onIncomingImage(CWImageObject imageObject);
        public void onRemoveImage(String fileid);
        public void onMoveImage(String fileid, double posx, double posy);
        public void onResizeImage(String fileid, double scale);
        public void onDisconnect();
        public void onError(String error, String cause);
        public void onImageSent(CWImageObject imageObject);
        public void onClearWall();
    }


    public static class CWImageObject {
        public String fileid;
        public double scale;
        public double normPosX;
        public double normPosY;
        public double width;
        public double height;
        public Bitmap thumbnail;

        public byte[] annotation;

        public CWImageObject(String fileid, double width, double height, double normPosX, double normPosY, double scale, Bitmap thumbnail) {
            this.fileid = fileid;
            this.normPosX = normPosX;
            this.normPosY = normPosY;
            this.scale = scale;
            this.thumbnail = thumbnail;
            this.width = width;
            this.height = height;
        }
    }

    Socket socket;
    Context context;
    String uri;
    String password;

    public ContextuwallAPI(String uri, String password, Context context, CallbackListener callback){
        this.context = context;
        this.listcallbacks = new ArrayList<>();
        this.listcallbacks.add(callback);
        authenticate_ok = false;
        isconnecting = false;
        this.uri = uri;
        this.password = password;


        cwannotid = "342254223";
        imageObjectCache = new ArrayList<>();
    }

    public void addListener(CallbackListener callback) {
        Log.d(TAG, "[addListener] " + callback.toString());
        this.listcallbacks.add(callback);
    }
    public void removeListener(CallbackListener callback) {
        Log.d(TAG, "[removeListener] " + callback.toString());
        this.listcallbacks.remove(callback);
    }

    private void init(String uri, final String passwordhash) {
        Log.d(TAG, "[init] initialising connection to " + uri + ", " + passwordhash + "..not connecting though");
        TrustManager[] tmgr = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                        System.out.println("TrustManager: checkClientTrusted");
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                        System.out.println("TrustManager: checkServerTrusted");
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                }
        };
        SSLContext ssl = null;
        try {
            ssl = SSLContext.getInstance("SSL");

            ssl.init(null, tmgr, null);

        } catch (NoSuchAlgorithmException e) {


        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        try {
            System.out.println("creating socket to " + uri);
            IO.setDefaultSSLContext(ssl);
            IO.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    System.out.println("verifying: " + s);
                    return true;
                }
            });

            socket = IO.socket(uri);

            //cwAPI = new ContextuwallAPI(socket, getApplicationContext());

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, "connected..sending authentication");
                isconnecting = false;
                cwAuthenticate(passwordhash);
                imageObjectCache = new ArrayList<CWImageObject>();

                // wait for 10 seconds for an authentication reply
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "waiting for auth reply");
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if(! authenticate_ok) {
                            Log.d(TAG, "auth reply timeout: disconnecting");
                            disconnect();
                        }
                    }
                }).start();
            }
        });
        socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.d(TAG, "[socket] got Event disconnected");

                for(CallbackListener callback : listcallbacks)
                    callback.onDisconnect();

                imageObjectCache.clear();
                socket.close();
            }

        });
        socket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                if(args[0] instanceof EngineIOException) {
                    String causeMessage = ((EngineIOException) args[0]).getCause().getMessage();
                    String errorMessage = ((EngineIOException) args[0]).getMessage();
                    Log.d(TAG, "[socket] got Connect Timeout: " + "Error: " + errorMessage + "Cause: " + causeMessage);
                    for (CallbackListener callback : listcallbacks)
                        callback.onError(errorMessage, causeMessage);
                } else if(args[0] instanceof Long) {
                    Log.d(TAG, "[socket] got Connect Timeout after " + (Long)args[0] + " seconds");
                    for (CallbackListener callback : listcallbacks)
                        callback.onError("[socket] got Connect Timeout after " + (Long)args[0] + " seconds", null);
                } else {
                    Log.d(TAG, "[socket] got Connect Timeout");
                    for (CallbackListener callback : listcallbacks)
                        callback.onError("[socket] got Connect Timeout", null);
                }

                socket.close();
                //socket = null;
            }

        });
        socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                if(args[0] instanceof SocketIOException) {
//                String causeMessage = ((SocketIOException)args[0]).getCause().getMessage();
                    String errorMessage = ((SocketIOException) args[0]).getMessage();
                    Log.d(TAG, "[socket] got Connect Error: " + "Error: " + errorMessage);
                    for (CallbackListener callback : listcallbacks)
                        callback.onError(errorMessage, null);
                } else if(args[0] instanceof EngineIOException){
                    String error = ((EngineIOException)args[0]).getMessage();
                    Log.d(TAG, "[socket]: " + error);
                    for (CallbackListener callback : listcallbacks)
                        callback.onError(error, null);
                } else {
                    Log.d(TAG, "[socket] got unknown Connect Error: ");
                    for (CallbackListener callback : listcallbacks)
                        callback.onError("got unknown Connect Error", null);

                }
                socket.close();
                //socket = null;
            }

        });




        /*
            CONTEXTUWALL LISTENER
         */
        socket.on(CW_ONAUTHENTICATEOK, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                authenticate_ok = true;
                isconnecting = false;
                for(CallbackListener callback : listcallbacks)
                    callback.onConnect();
                requestModelFromServer();
            }
        });

        socket.on(CW_INCOMINGSHADOWIMAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject data = (JSONObject)args[0];
                    Log.d(TAG, "got incoming image notify " + data.getString("filename"));
                    byte[] m_thumbnail64s = Base64.decode(data.getString("m_thumbnail64"), Base64.DEFAULT);
                    final Bitmap thumbnail = BitmapFactory.decodeByteArray(m_thumbnail64s, 0, m_thumbnail64s.length, null);
                    final String fileid = data.getString("filename");

                    JSONObject normcoord = data.getJSONObject("m_normalisedLocation");
                    double scale = data.getDouble("m_scale");
                    double w = data.getDouble("m_width");
                    double h = data.getDouble("m_height");
                    double x = normcoord.getDouble("X");
                    double y = normcoord.getDouble("Y");
                    CWImageObject imageobject = new CWImageObject(
                            fileid,
                            w,
                            h,
                            x,
                            y,
                            scale,
                            thumbnail
                    );
                    for(CallbackListener callback : listcallbacks) {
                        callback.onIncomingImage(imageobject);
                    }
                    imageObjectCache.add(imageobject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        socket.on(CW_MOVEIMAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject data = (JSONObject)args[0];
                    Log.d(TAG, "moving image " + data.getString("filename"));
                    final String fileid = data.getString("filename");
                    JSONObject normcoord = data.getJSONObject("m_normalisedLocation");
                    double x = normcoord.getDouble("X");
                    double y = normcoord.getDouble("Y");

                    for(CallbackListener callback : listcallbacks) {
                        callback.onMoveImage(fileid, x, y);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        socket.on(CW_RESIZEIMAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject data = (JSONObject)args[0];
                    Log.d(TAG, "resizing image " + data.getString("filename"));
                    final String fileid = data.getString("filename");
                    double scale = data.getDouble("m_scale");

                    for(CallbackListener callback : listcallbacks) {
                        callback.onResizeImage(fileid, scale);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        socket.on(CW_REMOVEIMAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject data = (JSONObject)args[0];
                    Log.d(TAG, "removing image " + data.getString("filename"));
                    final String fileid = data.getString("filename");

                    for(CallbackListener callback : listcallbacks) {
                        callback.onRemoveImage(fileid);
                    }

                    imageObjectCache.remove(getImageFromFileId(fileid));
//                    removeImageObjectFromCache(fileid);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        socket.on(CW_CLEARWALL, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, "clearing wall");
                imageObjectCache.clear();
                for(CallbackListener callback : listcallbacks) {
                    callback.onClearWall();
                }

            }
        });
    }

    public boolean isConnected() {

        if(socket != null) {
            return socket.connected() && authenticate_ok;
        }
        else
            return false;
    }

    public boolean isConnecting() {

        return isconnecting;
    }

    public void connect() {
        if(socket == null || !socket.connected())
            init(uri, password);

        isconnecting = true;
        socket.connect();
    }
    public void disconnect() {
        if(socket != null) {
            socket.disconnect();
            socket.close();
            isconnecting = false;
            authenticate_ok = false;
        }
    }


    public void cwAuthenticate(String passwordhash) {
        //authenticate
        JSONObject auth = new JSONObject();

        //md5 = Tools.getMd5String("foobar");
        try {

            JSONObject data = new JSONObject();
            data.put("password", passwordhash);

            Log.d(TAG, "sending authenticate: " + passwordhash);
            socket.emit("authenticate", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void cwSendImage (Uri imageUri, String fileId) {
        if(!isConnected())
            return;
        Log.d(TAG, "decoding file from path: " + imageUri.getPath());
        Bitmap orgBitmap = null;
        Bitmap bitmap = null;
        try {
            orgBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
            Log.d(TAG, "org bitmap: w/h: " + orgBitmap.getWidth() + "/" + orgBitmap.getHeight());
            for(CallbackListener callback : listcallbacks)
                callback.onProgress(new Long(10));
            int orientation = 1;
            Cursor cursor = context.getContentResolver().query(imageUri,
                    new String[]{MediaStore.Images.Media.ORIENTATION}, null, null, null);
            if(cursor != null) {
                cursor.moveToFirst();
                orientation = cursor.getInt(0);
            }

            Log.d(TAG, "image orientation: " + orientation);
            if(orientation != 0) {
                Log.d(TAG, "rotating bitmap");
                Matrix rotMatrix = new Matrix();
                rotMatrix.postRotate(orientation);


                //rotMatrix.postRotate(80);
                bitmap = Bitmap.createBitmap(orgBitmap, 0, 0, orgBitmap.getWidth(), orgBitmap.getHeight(), rotMatrix, false);
                Log.d(TAG, "rotated bitmap: w/h: " + bitmap.getWidth() + "/" + bitmap.getHeight());
                //orgBitmap.recycle();
            } else {
                Log.d(TAG, "not rotated bitmap");
                bitmap = orgBitmap;
            }
            for(CallbackListener callback : listcallbacks)
                callback.onProgress(new Long(20));

        } catch (IOException e) {
            e.printStackTrace();

        }
        float aspect = (float)bitmap.getWidth() / (float)bitmap.getHeight();
        Log.d(TAG, "loaded bitmap.. sending with aspect " + aspect);
//        Bitmap bitmap = BitmapFactory.decodeFile(imageUri.getPath());


        Bitmap thumbnail = Bitmap.createScaledBitmap(bitmap, 300, (int)(300.0f / aspect), false );
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 80, bos);
        String base64Thumbnail = new String(Base64.encode(bos.toByteArray(), Base64.DEFAULT));

        String filename = fileId + ".jpg";
        for(CallbackListener callback : listcallbacks)
            callback.onProgress(new Long(25));


        try {
            JSONObject data = new JSONObject();
            data.put("filename", filename);
            data.put("m_width", bitmap.getWidth());
            data.put("m_height", bitmap.getHeight());
            data.put("m_scale", 0.5);
            JSONObject pos = new JSONObject();
            pos.put("X", 0.5);
            pos.put("Y", 0.5);
            data.put("m_normalisedLocation", pos);
            data.put("m_thumbnail64", base64Thumbnail);


            socket.emit("incomingImageNotify", data);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        for(CallbackListener callback : listcallbacks)
            callback.onProgress(new Long(40));

        try {
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String type = context.getContentResolver().getType(imageUri);
        Log.d(TAG, "content type: " + type);

        Log.d(TAG, "creating base64 image");
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(imageUri);
            bos = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int len;
            while((len = inputStream.read(buffer)) != -1) {
                bos.write(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
        for(CallbackListener callback : listcallbacks)
            callback.onProgress(new Long(50));
//
        //Log.d(TAG, "compressing done");
        String base64Image = new String(Base64.encode(bos.toByteArray(), Base64.DEFAULT));
        for(CallbackListener callback : listcallbacks)
            callback.onProgress(new Long(70));

        Log.d(TAG, "encoding done");
        try {
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        bitmap.recycle();
        Log.d(TAG, "before sending fullimage size: " + base64Image.length());
        try {
            JSONObject data = new JSONObject();
            data.put("filename", filename);
            data.put("m_width", bitmap.getWidth());
            data.put("m_height", bitmap.getHeight());
            data.put("m_scale", 0.5);
            JSONObject pos = new JSONObject();
            pos.put("X", 0.5);
            pos.put("Y", 0.5);
            data.put("m_normalisedLocation", pos);
            data.put("m_image64", base64Image);
            data.put("m_thumbnail64", base64Thumbnail);


            Log.d(TAG, "emitting: incomingImage");
            socket.emit("incomingImage", data);
            Log.d(TAG, "after emitting: incomingImage");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        for(CallbackListener callback : listcallbacks)
            callback.onProgress(new Long(100));

        CWImageObject cwImageObject = new CWImageObject(
                filename,
                bitmap.getWidth(),
                bitmap.getHeight(),
                0.5,
                0.5,
                0.5,
                thumbnail
        );
        imageObjectCache.add(cwImageObject);
        for(CallbackListener callback : listcallbacks)
            callback.onImageSent(
                    cwImageObject
            );
    }

    public synchronized void sendAnnotation(String filename, byte[] pngimage){

        int sum = 0;
        byte[] oldannot = getImageFromFileId(filename).annotation;
        if(oldannot != null){
            int cnt = pngimage.length > oldannot.length ? oldannot.length : pngimage.length;
            for (int i = 0; i < cnt; i++) {
                sum += oldannot[i] - pngimage[i];
            }
            if (sum != 0)
                Log.d(TAG, "arrays different");
            else
                Log.d(TAG, "arrays same...");
        }
        getImageFromFileId(filename).annotation = pngimage;
        try {
            String[] fileparts = filename.split("\\.");
            JSONObject data = new JSONObject();
            data.put("imageFilename", filename);
            data.put("m_layer64", new String(Base64.encode(pngimage, Base64.NO_WRAP)));
            data.put("m_ownerID", socket.id());
            data.put("filename", fileparts[0] + "_annot_ " + socket.id() + fileparts[1]);


            Log.d(TAG, "emitting: incomingAnnotation");
            socket.emit("incomingAnnotation", data);
            Log.d(TAG, "after emitting: incomingAnnotation");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void requestModelFromServer() {
        Log.d(TAG, "[requestModelFromServer]");
        JSONObject data = new JSONObject();
        socket.emit("requestModel");
    }

    public List<CWImageObject> requestModel() {
        return imageObjectCache;
    }
    public void moveImage(String fileid, double posx, double posy) {
        try {
            JSONObject data = new JSONObject();
            JSONObject normPos = new JSONObject();
            normPos.put("X", posx);
            normPos.put("Y", posy);
            data.put("filename", fileid);
            data.put("m_normalisedLocation", normPos);
            socket.emit("updateImageLocation", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void scaleImage(String fileid, double scale) {
        try {
            JSONObject data = new JSONObject();

            data.put("filename", fileid);
            data.put("m_scale", scale);
            socket.emit("resizeImage", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void bringImageToFront(String fileid) {
        try {
            JSONObject data = new JSONObject();
            data.put("filename", fileid);
            socket.emit("bringImageToFront", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void removeImage(String fileid) {

        try {
            JSONObject data = new JSONObject();
            data.put("filename", fileid);
            socket.emit("removeImage", data);
            imageObjectCache.remove(getImageFromFileId(fileid));
//            removeImageObjectFromCache(fileid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void clearWall() {
        imageObjectCache.clear();
        socket.emit(CW_CLEARWALL);
    }



    public CWImageObject getImageFromFileId(String fileid) {
        for(CWImageObject o : imageObjectCache)
            if(o.fileid.equals(fileid))
                return o;
        return null;
    }

    private void removeImageObjectFromCache(String fileid) {
        Iterator<CWImageObject> iterator = imageObjectCache.iterator();

        while(iterator.hasNext()){
            CWImageObject next = iterator.next();
            if(next.fileid.equals(fileid)){
                imageObjectCache.remove(next);
                break;
            }
        }
    }
}
